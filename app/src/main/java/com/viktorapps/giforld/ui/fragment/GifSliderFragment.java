package com.viktorapps.giforld.ui.fragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.viktorapps.giforld.R;
import com.viktorapps.giforld.tenorapi.TenorClientService;
import com.viktorapps.giforld.tenorapi.model.TenorRequestParams;
import com.viktorapps.giforld.tenorapi.TenorRequester;
import com.viktorapps.giforld.tenorapi.model.TenorResponse;
import com.viktorapps.giforld.ui.MainActivity;
import com.viktorapps.giforld.util.ChangeShareIntentListener;
import com.viktorapps.giforld.util.GifDiffCallback;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GifSliderFragment extends Fragment implements TenorRequester, SearchView.OnQueryTextListener {
    private static final int OFFSET_SLIDER_ITEMS_COUNT = 16;
    private List<SliderItem> sliderItems = new LinkedList<>();
    private SliderAdapter sliderAdapter;
    private ViewPager2 viewPager2;
    private String SEARCH_TERM = TenorClientService.DEFAULT_SEARCH_TERM;
    private TenorRequester DEFAULT_TENOR_REQUESTER = this;
    private ChangeShareIntentListener onChangeShareIntentListener;
    private final Object sliderPrivateIOMutex = new Object();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gif_slider_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        requestTenorNewImages(null, null);

        viewPager2 = view.findViewById(R.id.viewPagerImageSlider);
        sliderAdapter = new SliderAdapter(sliderItems, viewPager2);
        sliderAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                Log.v("keko ", " on changed");
                super.onChanged();
            }
        });
        viewPager2.setAdapter(sliderAdapter);
        //attributes
        viewPager2.setOffscreenPageLimit(3);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        //transformer
        viewPager2.setPageTransformer(
                this.buildPageTransformer()
        );
        //callbacks
        viewPager2.registerOnPageChangeCallback(
                this.buildOnPageChangesCallListener()
        );


        super.onViewCreated(view, savedInstanceState);
    }

    private ViewPager2.OnPageChangeCallback buildOnPageChangesCallListener() {
        return new ViewPager2.OnPageChangeCallback() {

            @Override
            public void onPageSelected(int position) {
                if ((sliderItems.size() > OFFSET_SLIDER_ITEMS_COUNT * 4) && (position > sliderItems.size() / 2)) {
                    new DeleteFirstSliderElementsTask().execute(OFFSET_SLIDER_ITEMS_COUNT);

                }
                if (sliderItems.get(position) != null & sliderItems.get(position).getUrl() != null) {
                    onChangeShareIntentListener.onChangeShareIntent(sliderItems.get(position).getUrl());
                }
                if ((position + 2 > sliderItems.size()
                        && sliderItems.size() < OFFSET_SLIDER_ITEMS_COUNT * 2)
                        || sliderItems.size() - position == OFFSET_SLIDER_ITEMS_COUNT) {
                    requestTenorNewImages(null, null);
                }
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        };
    }

    private void requestTenorNewImages(@Nullable TenorRequester requester, @Nullable TenorRequestParams params) {
        TenorClientService
                .getAccess()
                .requestTenorUpdate(
                        requester != null ? requester : DEFAULT_TENOR_REQUESTER,
                        params != null ? params : new TenorRequestParams(SEARCH_TERM, OFFSET_SLIDER_ITEMS_COUNT));
    }

    private CompositePageTransformer buildPageTransformer() {
        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(1));
        return compositePageTransformer;
    }

    @Override
    public void onTenorReturnResponse(@NonNull TenorResponse tenorResponse) {
        new AddNewSliderElementsTask().execute(tenorResponse);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        GifSliderFragment.this.SEARCH_TERM = query;
        MenuItem searchMenuItem = MainActivity.getMenu().findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setQueryHint(query);
        searchMenuItem.collapseActionView();
        viewPager2.setCurrentItem(sliderItems.size(), true);
        requestTenorNewImages(
                null,
                null);
        return false;
    }

    public void setOnChangeShareIntentListener(ChangeShareIntentListener target) {
        this.onChangeShareIntentListener = target;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @SuppressLint("StaticFieldLeak")
    private class AddNewSliderElementsTask extends AsyncTask<TenorResponse, List<SliderItem>, List<SliderItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected final List<SliderItem> doInBackground(TenorResponse... response) {
            synchronized (sliderPrivateIOMutex) {
                List<SliderItem> bufList = new ArrayList<>(sliderItems);
                response[0].getResults().forEach(
                        result -> result.getMedia().forEach(
                                (media) -> {
                                    bufList.add(new SliderItem(media.getGif().getUrl()));
                                    Log.v("GIF url: ", media.getGif().getUrl());
                                }));

                return bufList;
            }
        }

        @Override
        protected void onPostExecute(List<SliderItem> newList) {
            updateAdapterDataList(newList);
            super.onPostExecute(newList);
        }
    }

    private void updateAdapterDataList(List<SliderItem> newList) {
        boolean needToScroll = sliderItems.size() == 0;
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new GifDiffCallback<>(this.sliderItems, newList), true);
        sliderAdapter.setData(newList);
        sliderItems = newList;
        diffResult.dispatchUpdatesTo(sliderAdapter);
        if (needToScroll) viewPager2.setCurrentItem(1, true);
    }


    @SuppressLint("StaticFieldLeak")
    private class DeleteFirstSliderElementsTask extends AsyncTask<Integer, List<SliderItem>, List<SliderItem>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<SliderItem> doInBackground(Integer... countToDelete) {
            synchronized (sliderPrivateIOMutex) {
                List<SliderItem> bufList = new ArrayList<>(sliderItems);
                bufList.removeIf(sliderItem -> bufList.indexOf(sliderItem) < countToDelete[0]);
                return bufList;
            }
        }

        @Override
        protected void onPostExecute(List<SliderItem> newList) {
            updateAdapterDataList(newList);
            super.onPostExecute(newList);
        }
    }
}
