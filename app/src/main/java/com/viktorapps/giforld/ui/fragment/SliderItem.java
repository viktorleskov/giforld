package com.viktorapps.giforld.ui.fragment;

import androidx.annotation.Nullable;

public class SliderItem {
    private static long counter = 0;
    private String url;
    private int resource_id;
    private long item_id;

    public SliderItem(String url) {
        this.item_id = counter;
        counter++;
        this.url = url;
    }

    public SliderItem(int resource_id) {
        this.item_id = counter;
        counter++;
        this.resource_id = resource_id;
    }

    @Override
    public int hashCode() {
        return (int) item_id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return this.hashCode() == obj.hashCode();
    }

    public int getResource_id() {
        return resource_id;
    }

    public String getUrl() {
        return url;
    }

    public long getItem_id() {
        return item_id;
    }
}
