package com.viktorapps.giforld.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.viktorapps.giforld.R;
import com.viktorapps.giforld.tenorapi.TenorClientService;
import com.viktorapps.giforld.util.ChangeShareIntentListener;
import com.viktorapps.giforld.ui.fragment.GifSliderFragment;

public class MainActivity extends AppCompatActivity implements ChangeShareIntentListener {
    private ShareActionProvider shareActionProvider;
    private ActionBar actionBar;
    private FragmentTransaction transaction;
    private GifSliderFragment gifSlider;
    private static Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        enterFullScreenFlags();
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        gifSlider = new GifSliderFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.gif_slider_container, gifSlider);
        transaction.commit();

        gifSlider.setOnChangeShareIntentListener(this);


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        MainActivity.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView
                .setQueryHint(getResources().getString(R.string.search_hint));
        searchView
                .setOnQueryTextListener(gifSlider);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void enterFullScreenFlags() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow()
                .setFlags(
                        WindowManager
                                .LayoutParams
                                .FLAG_FULLSCREEN,
                        WindowManager
                                .LayoutParams
                                .FLAG_FULLSCREEN);
    }

    public static Menu getMenu() {
        return menu;
    }

    @Override
    public void onChangeShareIntent(String text) {
        shareActionProvider.refreshVisibility();
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareActionProvider.setShareIntent(sendIntent);
    }
}
