package com.viktorapps.giforld.tenorapi;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.viktorapps.giforld.tenorapi.model.TenorRequestParams;
import com.viktorapps.giforld.tenorapi.model.TenorResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

public class TenorClientService {
    /**
     * private
     */
    private static final String TENOR_SEARCH_QUERY = "https://api.tenor.com/v1/random?q=%1$s&key=%2$s&limit=%3$s&pos=%4$s";
    private static final Object tenorPrivateMutex = new Object();
    /**
     * Tenor developer api key for my email
     *
     * @email (viktorius488 @ gmail.com)
     */
    private static final String API_KEY = "78L5HGR4Z7MV";
    /**
     * public constants
     */
    public static final int DEFAULT_SEARCH_LIMIT = 10;
    public static final String DEFAULT_SEARCH_TERM = "excited";

    /**
     * state
     */
    private static TenorClientService tenorStaticAccess;
    private static Set<TenorRequester> requesterSet;
    private static volatile String next = "";
    private static String last_search_term;

    public static TenorClientService getAccess() {
        if (tenorStaticAccess != null) return tenorStaticAccess;
        tenorStaticAccess = new TenorClientService();
        return tenorStaticAccess;
    }

    public void requestTenorUpdate(TenorRequester requester, TenorRequestParams tenorRequestParams) {
        registerRequester(requester);
        synchronized (tenorPrivateMutex) {
            new AsyncHttpExecutor().execute(tenorRequestParams);
        }
    }

    private void registerRequester(TenorRequester requester) {
        if (TenorClientService.requesterSet == null) {
            TenorClientService.requesterSet = new LinkedHashSet<>();
        }
        TenorClientService.requesterSet.add(requester);
    }

    private static void onResponseForAllRequester(TenorResponse response) {
        if (response != null) {
            requesterSet.forEach(requester -> requester.onTenorReturnResponse(response));
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncHttpExecutor extends AsyncTask<TenorRequestParams, TenorResponse, TenorResponse> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected TenorResponse doInBackground(TenorRequestParams... params) {
            synchronized (tenorPrivateMutex) {
                StrictMode.ThreadPolicy policy = new
                        StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                final String searchTerm = params[0].getSearchTerm();
                if (!searchTerm.equals(last_search_term)) next = "";
                last_search_term = searchTerm;
                return getTenorSearchResult(last_search_term, params[0].getSearchLimit());
            }
        }

        @Override
        protected void onPostExecute(TenorResponse result) {
            if (result == null) return;
            next = result.getNext();
            onResponseForAllRequester(result);
            super.onPostExecute(result);
        }

        private TenorResponse getTenorSearchResult(String searchTerm, int limit) {
            TenorResponse tenorResponse;
            Gson gson = new Gson();
            JSONObject searchResult = getSearchResult(searchTerm, limit);
            try {
                tenorResponse = gson.fromJson(String.valueOf(searchResult), TenorResponse.class);
                return tenorResponse;
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
            return null;
        }

        private JSONObject getSearchResult(String searchTerm, int limit) {
            final String url = String.format(TENOR_SEARCH_QUERY,
                    searchTerm, API_KEY, limit, next);
            try {
                Log.v("Request URL :", url);
                return get(url);
            } catch (IOException | JSONException ignored) {
            }
            return null;
        }

        private JSONObject get(String url) throws IOException, JSONException {
            HttpURLConnection connection = null;
            try {
                connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                int statusCode = connection.getResponseCode();
                if (statusCode != HttpURLConnection.HTTP_OK && statusCode != HttpURLConnection.HTTP_CREATED) {
                    String error = String.format("HTTP Code: '%1$s' from '%2$s'", statusCode, url);
                    throw new ConnectException(error);
                }
                return parser(connection);
            } catch (Exception ignored) {
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return new JSONObject("");
        }

        private JSONObject parser(HttpURLConnection connection) throws JSONException {
            char[] buffer = new char[1024 * 4];
            int n;
            try (InputStream stream = new BufferedInputStream(connection.getInputStream())) {
                InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
                StringWriter writer = new StringWriter();
                while (-1 != (n = reader.read(buffer))) {
                    writer.write(buffer, 0, n);
                }
                return new JSONObject(writer.toString());
            } catch (IOException ignored) {
            }
            return new JSONObject("");
        }
    }
}
