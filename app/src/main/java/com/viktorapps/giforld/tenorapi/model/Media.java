package com.viktorapps.giforld.tenorapi.model;

public class Media {
    MediaItem nanomp4;
    MediaItem nanowebm;
    MediaItem tinygif;
    MediaItem tinymp4;
    MediaItem tinywebm;
    MediaItem webm;
    MediaItem gif;
    MediaItem mp4;
    MediaItem loopedmp4;
    MediaItem mediumgif;
    MediaItem nanogif;

    public Media(MediaItem nanomp4, MediaItem nanowebm, MediaItem tinygif, MediaItem tinymp4, MediaItem tinywebm, MediaItem webm, MediaItem gif, MediaItem mp4, MediaItem loopedmp4, MediaItem mediumgif, MediaItem nanogif) {
        this.nanomp4 = nanomp4;
        this.nanowebm = nanowebm;
        this.tinygif = tinygif;
        this.tinymp4 = tinymp4;
        this.tinywebm = tinywebm;
        this.webm = webm;
        this.gif = gif;
        this.mp4 = mp4;
        this.loopedmp4 = loopedmp4;
        this.mediumgif = mediumgif;
        this.nanogif = nanogif;
    }

    public MediaItem getNanomp4() {
        return nanomp4;
    }

    public MediaItem getNanowebm() {
        return nanowebm;
    }

    public MediaItem getTinygif() {
        return tinygif;
    }

    public MediaItem getTinymp4() {
        return tinymp4;
    }

    public MediaItem getTinywebm() {
        return tinywebm;
    }

    public MediaItem getWebm() {
        return webm;
    }

    public MediaItem getGif() {
        return gif;
    }

    public MediaItem getMp4() {
        return mp4;
    }

    public MediaItem getLoopedmp4() {
        return loopedmp4;
    }

    public MediaItem getMediumgif() {
        return mediumgif;
    }

    public MediaItem getNanogif() {
        return nanogif;
    }
}
