package com.viktorapps.giforld.tenorapi.model;

import java.util.List;

public class TenorResponse {
    private String weburl;
    private List<Result> results;
    private String next;

    public TenorResponse(List<Result> results, String next) {
        this.next = next;
        this.results = results;
    }

    public List<Result> getResults() {
        return results;
    }

    public String getNext() {
        return next;
    }
}
