package com.viktorapps.giforld.tenorapi.model;

public class MediaItem {
    String url;
    Integer[] dims;
    Double duration;
    String preview;
    Long size;

    public MediaItem(String url, Integer[] dims, Double duration, String preview, Long size) {
        this.url = url;
        this.dims = dims;
        this.duration = duration;
        this.preview = preview;
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public Integer[] getDims() {
        return dims;
    }

    public Double getDuration() {
        return duration;
    }

    public String getPreview() {
        return preview;
    }

    public Long getSize() {
        return size;
    }
}
