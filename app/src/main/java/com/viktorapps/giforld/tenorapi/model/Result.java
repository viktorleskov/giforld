package com.viktorapps.giforld.tenorapi.model;

import java.util.List;

public class Result {
    Boolean hascaption;
    List<String> tags;
    String url;
    List<Media> media;
    Double created;
    Long shares;
    String itemurl;
    Object composite;
    Boolean hasaudio;
    String title;
    Long id;

    public Result(Boolean hascaption, List<String> tags, String url, List<Media> media, Double created, Long shares, String itemurl, Object composite, Boolean hasaudio, String title, Long id) {
        this.hascaption = hascaption;
        this.tags = tags;
        this.url = url;
        this.media = media;
        this.created = created;
        this.shares = shares;
        this.itemurl = itemurl;
        this.composite = composite;
        this.hasaudio = hasaudio;
        this.title = title;
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getUrl() {
        return url;
    }

    public List<Media> getMedia() {
        return media;
    }

    public Double getCreated() {
        return created;
    }

    public Long getShares() {
        return shares;
    }

    public String getItemurl() {
        return itemurl;
    }

    public Object getComposite() {
        return composite;
    }

    public Boolean getHasaudio() {
        return hasaudio;
    }

    public String getTitle() {
        return title;
    }

    public Long getId() {
        return id;
    }
}
