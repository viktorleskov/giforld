package com.viktorapps.giforld.tenorapi;

import com.viktorapps.giforld.tenorapi.model.TenorResponse;

public interface TenorRequester {
    void onTenorReturnResponse(TenorResponse tenorResponse);
}
