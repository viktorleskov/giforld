package com.viktorapps.giforld.tenorapi.model;

public class TenorRequestParams {
    private String searchTerm;
    private int searchLimit;

    public TenorRequestParams(String searchTerm, int searchLimit) {
        this.searchTerm = searchTerm;
        this.searchLimit = searchLimit;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public int getSearchLimit() {
        return searchLimit;
    }
}
