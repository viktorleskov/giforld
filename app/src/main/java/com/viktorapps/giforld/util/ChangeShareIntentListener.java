package com.viktorapps.giforld.util;

public interface ChangeShareIntentListener {
    public void onChangeShareIntent(String text);
}
