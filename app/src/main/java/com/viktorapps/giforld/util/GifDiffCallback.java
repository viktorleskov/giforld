package com.viktorapps.giforld.util;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public class GifDiffCallback<E> extends DiffUtil.Callback {
    List<E> oldItems;
    List<E> newItems;

    public GifDiffCallback(List<E> oldItems, List<E> newItems) {
        this.newItems = newItems;
        this.oldItems = oldItems;
    }

    @Override
    public int getOldListSize() {
        Log.v("Old list size: ", "" + oldItems.size());
        return oldItems.size();
    }

    @Override
    public int getNewListSize() {
        Log.v("New list size: ", "" + newItems.size());
        return newItems.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldItems.get(oldItemPosition).hashCode() == newItems.get(newItemPosition).hashCode();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldItems.get(oldItemPosition).equals(newItems.get(newItemPosition).hashCode());
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
